#!/bin/bash
# первый аргумент - это путь до дириктории с файлами
# второй аргумент - это название конечного файла

path=$(pwd)
files=$(ls $1)

cd $1

for t in ${files[@]}; do
    echo -e $(cat $t) >> $path/$2
    echo -e '' >> $path/$2
done
